# CP_Heur_Paper_Evalutation

This repository contains all generated instances involved in testing and their solutions achieved by various methods for paper "Constraint Programming and Constructive Heuristics for Parallel Machine Scheduling with Sequence-Dependent Setups Executed".

## File structure

- **comparison_our_approaches** (*Contains everything related to comparison of our proposed approaches.*)
    - instances
        - Contains problem instances used for evaluation in name format: **m<machine_count>_n<task_count>.txt**. Number of servers is not considered since it has no effect on instance structure.
    - solutions
        - Contains solutions of problem instances in name format: **<approach_used>_<machine_cout>_n<task_count>_w<server_count>.txt**. This time, number of servers is considered since it influences the solution.
    - <BATCH_FILES>
        - Each file containing sum up of the whole testing.
- **comparison_papers** (*Contains everything related to comparison to existing papers. Each paper has its own folder denoted by first letters of the authors.*)
    - exact_instances
        - Contains problem instances used for exact approach comparison evaluation in name format: **m<machine_count>_n<task_count>.txt**. Number of servers is not considered since it has no effect on instance structure.


## Instance format
Instance txt file contains "<machine_count>, <task_count>, <array_of_task_processing_times>, <2D_array_of_setup_times_between_every_task_pair>. The arrays are indexed by task indexes as described in the paper.

## Solution format
Solution txt file contains two rows defining number of machines, tasks and servers and then for each task it contains a row with index of machine to which it is assigned, id of task, start time of task, setup length and processing time. Last row contains objective value and execution time in milliseconds.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Authors
Vilém Heinz, Antonín Novák, Marek Vlk and Zdeněk Hanzálek

## License
Feel free to use generated instances in your research.

## Issues
Any error please report to heinzvil@fel.cvut.cz
